const pattern1 = "https://vmware-gs.lightning.force.com/lightning/*";
const filter = {urls: [pattern1]}
var tabToHandle = 0;

function handleUpdated(tabId, changeInfo, tabInfo) {
if(tabToHandle == `${tabId}`){
    getCurrentWindowTabs().then((tabs) => {
      var indexArr = []
      for(let tab of tabs){
        indexArr.push(tab.id);
      }
      const firstId = Math.min(...indexArr);
      if(firstId != tabToHandle){
        browser.tabs.update(firstId, {active: true});
        browser.tabs.update(firstId, {url: tabInfo.url});
        var hiding = browser.tabs.hide(tabToHandle);
        var discarding = browser.tabs.remove(tabToHandle);
      }
    });
    return;
  }
}

function handleCreated(tab){
  //handling newly created tab only
  tabToHandle = tab.id;
}

//get list of tabs in window
function getCurrentWindowTabs() {
  return browser.tabs.query({currentWindow: true, url: "https://vmware-gs.lightning.force.com/lightning/*", hidden: false});
}
 
browser.tabs.onUpdated.addListener(handleUpdated, filter);
browser.tabs.onCreated.addListener(handleCreated);