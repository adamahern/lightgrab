# LightGrab
![LightGrab Logo](/icons/grab96.jpg)

## Description
GS Lightning link grabber. Opens cases in an existing tab instead of a new one.

You can install this addon from its product page:
https://addons.mozilla.org/en-US/firefox/addon/lightgrab/

Firefox only. Download:
https://www.mozilla.org/en-GB/firefox/new/


